/*
    Asela Pathirage
    Index no: 19020538
*/

#include <stdio.h>
/* Program that prints the fibonacci sequence up to a given number */

/*recursive function which prints fibonacci sequence*/
void fibonacciSeq(int n);

/*recursive function which calculates fibonacci number*/
int fibonacci(int n);

int fibonacci(int n){
    if(n==0) return 0;
    if(n==1) return 1;

    return fibonacci(n-1) + fibonacci(n-2);
}

void fibonacciSeq(int n){
    if(n>=0){
        fibonacciSeq(n-1);
        printf("%d\n",fibonacci(n));
    }
}

int main(){
    int n;
    /*getting user inputs for a number */
    printf("Enter an integer: ");
    scanf("%d",&n);

    /*printing the fibonacci sequence*/
    printf("Fibonacci Sequence up to %d \n",n);
    fibonacciSeq(n);

    return 0;
}
