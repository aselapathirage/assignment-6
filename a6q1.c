/*
    Asela Pathirage
    Index no: 19020538
*/

#include <stdio.h>
/* Program that prints a number triangle using recursive function */

/*recursive function which prints a number triangle*/
void pattern(int n);

void pattern(int n){
    if(n>0){
        pattern(n-1);
        while(n>0){
            printf("%d",n);
            n--;
        }
        printf("\n");
    }
}

int main(){
    int n;
    /*getting user inputs for a number */
    printf("Enter an integer: ");
    scanf("%d",&n);
    pattern(n);

    return 0;
}
